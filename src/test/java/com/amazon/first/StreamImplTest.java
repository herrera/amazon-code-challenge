package com.amazon.first;

import org.junit.Assert;
import org.junit.Test;

public class StreamImplTest {

	@Test
	public void nullSequeceShouldProduceAnEmptyStream() {
		Stream stream = new StreamImpl(null);
		Assert.assertFalse(stream.hasNext());
	}

	@Test
	public void emptySequeceShouldProduceAnEmptyStream() {
		Stream stream = new StreamImpl("");
		Assert.assertFalse(stream.hasNext());
	}

	@Test
	public void nonEmptySequeceShouldProduceAnNonEmptyStream() {
		Stream stream = new StreamImpl("test");
		Assert.assertTrue(stream.hasNext());
	}

	@Test
	public void testStreamEmptiness() {
		Stream stream = new StreamImpl("test");
		for (int i = 0; i < 4; ++i) {
			Assert.assertTrue(stream.hasNext());
			stream.getNext();
		}
		Assert.assertFalse(stream.hasNext());
	}

	@Test
	public void shouldReturnNULLWhenSequenceIsNull() {
		Stream stream = new StreamImpl(null);
		Assert.assertEquals(Stream.NULL, stream.getNext());
	}

	@Test
	public void shouldReturnNULLWhenSequenceIsEmpty() {
		Stream stream = new StreamImpl("");
		Assert.assertEquals(Stream.NULL, stream.getNext());
	}

	@Test
	public void testSequeceShouldIterateOverTheCharsContainedIntoTheTestWord() {
		char[] test = new char[4];
		Stream stream = new StreamImpl("test");
		int i = 0;
		while (stream.hasNext()) {
			test[i++] = stream.getNext();
		}
		Assert.assertArrayEquals(new char[] { 't', 'e', 's', 't' }, test);
	}
}
