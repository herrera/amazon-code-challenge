package com.amazon.first;

import org.junit.Assert;
import org.junit.Test;

public class NonRepeatibleTest {

	@Test
	public void shouldReturnTheFirstNonRepeatibleCharE() {
		char firstNonRepeatableChar = 'e';
		Stream stream  =  new StreamImpl("aaaaBBBBccccccDDDDDDDDeFFFFFFFFFF");
		NonRepeatable nonRepeatable = new NonRepeatable();
		Assert.assertEquals(firstNonRepeatableChar, nonRepeatable.firstChar(stream));
	}

	@Test
	public void shouldReturnTheFirstNonRepeatibleChar0() {
		char firstNonRepeatableChar = '0';
		Stream stream  =  new StreamImpl("aaaaBBBBcccccc0DDDDDDDDeFFFFFFFFFF");
		NonRepeatable nonRepeatable = new NonRepeatable();
		Assert.assertEquals(firstNonRepeatableChar, nonRepeatable.firstChar(stream));
	}

	@Test
	public void shouldReturnTheFirstNonRepeatibleCharZ() {
		char firstNonRepeatableChar = 'Z';
		Stream stream  =  new StreamImpl("ZaaaaBBBBcccccc0DDDDDDDDeFFFFFFFFFF");
		NonRepeatable nonRepeatable = new NonRepeatable();
		Assert.assertEquals(firstNonRepeatableChar, nonRepeatable.firstChar(stream));
	}

	@Test
	public void shouldReturnNullCharBecauseAllCharsAreRepeated() {
		char firstNonRepeatableChar = Stream.NULL;
		Stream stream  =  new StreamImpl("aaaaBBBBccccccDDDDDDDDFFFFFFFFFF");
		NonRepeatable nonRepeatable = new NonRepeatable();
		Assert.assertEquals(firstNonRepeatableChar, nonRepeatable.firstChar(stream));
	}
}
