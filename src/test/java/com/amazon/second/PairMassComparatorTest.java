package com.amazon.second;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class PairMassComparatorTest {

	@Test
	public void massOfPairAShouldBeGreaterThanstrengthOfPairB() throws Exception {
		Pair pairA = new Pair(140, 100);
		Pair pairB = new Pair(130, 100);
		Assert.assertEquals(1, new PairMassComparator().compare(pairA, pairB));
	}

	@Test
	public void massOfPairAShouldBeLesserThanstrengthOfPairB() throws Exception {
		Pair pairA = new Pair(130, 100);
		Pair pairB = new Pair(140, 100);
		Assert.assertEquals(-1, new PairMassComparator().compare(pairA, pairB));
	}

	@Test
	public void massOfAthleteAShouldBeEqualsToAthleteB() throws Exception {
		Pair pairA = new Pair(100, 100);
		Pair pairB = new Pair(100, 100);
		Assert.assertEquals(0, new PairMassComparator().compare(pairA, pairB));
	}

	@Test
	public void shouldOrderMasses() throws Exception {
		Pair pairA = new Pair(140, 100);
		Pair pairB = new Pair(130, 100);
		Pair pairC = new Pair(120, 100);
		Pair pairD = new Pair(110, 100);
		Pair pairE = new Pair(100, 100);
		List<Pair> pairs = Arrays.asList(pairA, pairB, pairC, pairD, pairE);
		Collections.sort(pairs, new PairMassComparator());
		Assert.assertEquals(pairA, pairs.get(4));
		Assert.assertEquals(pairB, pairs.get(3));
		Assert.assertEquals(pairC, pairs.get(2));
		Assert.assertEquals(pairD, pairs.get(1));
		Assert.assertEquals(pairE, pairs.get(0));
	}
}
