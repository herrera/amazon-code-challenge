package com.amazon.second;

import org.junit.Assert;
import org.junit.Test;

public class PairTest {

	@Test(expected=Exception.class)
	public void ensureThatCannotCreateMassWithAValueGreaterThanItsMax() throws Exception {
		new Pair(Pair.MAX_MASS + 1, Pair.MAX_STRENGTH);
	}

	@Test(expected=Exception.class)
	public void ensureThatCannotCreateStrengthWithAValueGreaterThanItsMax() throws Exception {
		new Pair(Pair.MAX_MASS, Pair.MAX_STRENGTH + 1);
	}

	@Test
	public void ensureThatCanCreatePairWhenBothMassAndStrengthAreEqualsToItsMax() throws Exception {
		Pair pair = new Pair(Pair.MAX_MASS, Pair.MAX_STRENGTH);
		Assert.assertEquals(Pair.MAX_MASS, pair.getMass());
		Assert.assertEquals(Pair.MAX_STRENGTH, pair.getStrength());
	}

	@Test
	public void ensureThatCanCreatePairWhenBothMassAndStrengthAreLesserToItsMax() throws Exception {
		Pair pair = new Pair(Pair.MAX_MASS - 1, Pair.MAX_STRENGTH - 1);
		Assert.assertEquals(Pair.MAX_MASS - 1, pair.getMass());
		Assert.assertEquals(Pair.MAX_STRENGTH - 1, pair.getStrength());
	}

	@Test
	public void ensureThatCanCreatePairWhenMassOrStrengthAreLesserToItsMax() throws Exception {
		Pair pairA = new Pair(Pair.MAX_MASS - 1, Pair.MAX_STRENGTH);
		Assert.assertEquals(Pair.MAX_MASS - 1, pairA.getMass());
		Pair pairB = new Pair(Pair.MAX_MASS, Pair.MAX_STRENGTH - 1);
		Assert.assertEquals(Pair.MAX_STRENGTH - 1, pairB.getStrength());
	}

	@Test
	public void shouldBeProperlyFormattedAsString() throws Exception {
		Pair pair = new Pair(10, 20);
		Assert.assertEquals("mass : '10', strength : '20'", pair.toString());
	}
}
