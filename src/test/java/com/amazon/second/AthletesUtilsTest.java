package com.amazon.second;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class AthletesUtilsTest {

	@Test(expected=Exception.class)
	public void maxAthletesConstantShouldDenyTheGetMaxTowerLengthMethodExecution() throws Exception {
		List<Athlete> athletes = new ArrayList<Athlete>();
		for (int i = 0; i < AthletesUtils.MAX_ATHLETES + 1; ++i) {
			athletes.add(new Athlete(new Pair(100, 100)));
		}
		new AthletesUtils().getMaxTowerLength(athletes);
	}

	@Test
	public void shouldSortAtlhetesInAscendingOrderByItsMasses() throws Exception {
		List<Athlete> athletes = new ArrayList<Athlete>();
		athletes.add(new Athlete(new Pair(150, 100)));
		athletes.add(new Athlete(new Pair(140, 100)));
		athletes.add(new Athlete(new Pair(130, 100)));
		athletes.add(new Athlete(new Pair(120, 100)));
		athletes.add(new Athlete(new Pair(110, 100)));
		List<Athlete> ascendingOrderedOnes = new AthletesUtils().getAscendingOrderedByMass(athletes);
		Assert.assertEquals(athletes.get(0), ascendingOrderedOnes.get(4));
		Assert.assertEquals(athletes.get(1), ascendingOrderedOnes.get(3));
		Assert.assertEquals(athletes.get(2), ascendingOrderedOnes.get(2));
		Assert.assertEquals(athletes.get(3), ascendingOrderedOnes.get(1));
		Assert.assertEquals(athletes.get(4), ascendingOrderedOnes.get(0));
	}

	@Test
	public void shouldGetTheStrongestAthlete() throws Exception {
		List<Athlete> athletes = new ArrayList<Athlete>();
		athletes.add(new Athlete(new Pair(100, 110)));
		athletes.add(new Athlete(new Pair(100, 150)));
		athletes.add(new Athlete(new Pair(100, 120)));
		athletes.add(new Athlete(new Pair(100, 105)));
		athletes.add(new Athlete(new Pair(100, 100)));
		Assert.assertEquals(athletes.get(1), new AthletesUtils().getStrongestOne(athletes));
	}

	@Test
	public void shouldGetNullStrongestAthleteCauseThereIsNoAthleteInList() throws Exception {
		List<Athlete> athletes = new ArrayList<Athlete>();
		Assert.assertNull(new AthletesUtils().getStrongestOne(athletes));
	}

	@Test
	public void shouldGetTheSumOfAthletesMasses() throws Exception {
		List<Athlete> athletes = new ArrayList<Athlete>();
		athletes.add(new Athlete(new Pair(100, 100)));
		athletes.add(new Athlete(new Pair(101, 100)));
		athletes.add(new Athlete(new Pair(102, 100)));
		athletes.add(new Athlete(new Pair(103, 105)));
		athletes.add(new Athlete(new Pair(104, 100)));
		Assert.assertEquals(510, new AthletesUtils().getSumOfMasses(athletes));
	}

	@Test
	public void shoudGetAZeroedSumOfMasses() {
		List<Athlete> athletes = new ArrayList<Athlete>();
		Assert.assertEquals(0, new AthletesUtils().getSumOfMasses(athletes));
	}

	@Test
	public void strongestAthleteShouldNotBeingAbleToHoldAnotherAthlete() throws Exception {
		List<Athlete> athletes = new ArrayList<Athlete>();
		athletes.add(new Athlete(new Pair(2, 1)));
		athletes.add(new Athlete(new Pair(2, 0)));
		athletes.add(new Athlete(new Pair(2, 0)));
		athletes.add(new Athlete(new Pair(2, 0)));
		Assert.assertEquals(1, new AthletesUtils().getMaxTowerLength(athletes));
	}

	@Test
	public void shouldGetTheMaxTowerLength() throws Exception {
		List<Athlete> athletes = new ArrayList<Athlete>();
		athletes.add(new Athlete(new Pair(3, 4)));
		athletes.add(new Athlete(new Pair(2, 2)));
		athletes.add(new Athlete(new Pair(7, 6)));
		athletes.add(new Athlete(new Pair(4, 5)));
		Assert.assertEquals(3, new AthletesUtils().getMaxTowerLength(athletes));
	}
}
