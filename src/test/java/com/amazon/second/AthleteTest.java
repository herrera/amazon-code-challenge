package com.amazon.second;

import org.junit.Assert;
import org.junit.Test;


public class AthleteTest {

	@Test
	public void ensureThatAnAthleteCanNotHoldMoreThanHisStrentghAllows() throws Exception {
		Athlete athleteA = new Athlete(new Pair(100, 120));
		Athlete athleteB = new Athlete(new Pair(150, 170));
		Assert.assertFalse(athleteA.canHold(athleteB));
	}

	@Test
	public void ensureThatAnAthleteCanHoldWhatHisStrentghAllows() throws Exception {
		Athlete athleteA = new Athlete(new Pair(100, 120));
		Athlete athleteB = new Athlete(new Pair(120, 150));
		Assert.assertTrue(athleteA.canHold(athleteB));
	}

	@Test
	public void athletesShouldBeCompletelyDifferentBetweenThemselves() throws Exception {
		Athlete athleteA = new Athlete(new Pair(1, 2));
		Athlete athleteB = new Athlete(new Pair(3, 4));
		Assert.assertFalse(athleteA.equals(athleteB));
	}

	@Test
	public void athletesShouldBeDifferentBetweenThemselvesByTheMass() throws Exception {
		Athlete athleteA = new Athlete(new Pair(1, 0));
		Athlete athleteB = new Athlete(new Pair(0, 0));
		Assert.assertFalse(athleteA.equals(athleteB));
	}

	@Test
	public void athletesShouldBeDifferentBetweenThemselvesByTheStrength() throws Exception {
		Athlete athleteA = new Athlete(new Pair(0, 1));
		Athlete athleteB = new Athlete(new Pair(0, 0));
		Assert.assertFalse(athleteA.equals(athleteB));
	}

	@Test
	public void athletesShouldNotBeEqualsCauseItIsComparedWithAnotherObjectType() throws Exception {
		Assert.assertFalse(new Athlete(new Pair(100, 120)).equals(new Object()));
	}

	@Test
	public void athletesShouldBeEquals() throws Exception {
		Athlete athleteA = new Athlete(new Pair(100, 120));
		Athlete athleteB = new Athlete(new Pair(100, 120));
		Assert.assertTrue(athleteA.equals(athleteB));
	}

	@Test
	public void shouldGetProperlyStringRepresentation() throws Exception {
		Athlete athlete = new Athlete(new Pair(100, 120));
		Assert.assertEquals("mass : '100', strength : '120'", athlete.toString());
	}
}
