package com.amazon.second;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class AthleteMassComparatorTest {

	@Test
	public void massOfAthleteAShouldBeGreaterThanMassOfAthleteB() throws Exception {
		Athlete athleteA = new Athlete(new Pair(140, 100));
		Athlete athleteB = new Athlete(new Pair(130, 100));
		Assert.assertEquals(1, new AthleteMassComparator().compare(athleteA, athleteB));
	}

	@Test
	public void massOfAthleteAShouldBeLesserThanMassOfAthleteB() throws Exception {
		Athlete athleteA = new Athlete(new Pair(130, 100));
		Athlete athleteB = new Athlete(new Pair(140, 100));
		Assert.assertEquals(-1, new AthleteMassComparator().compare(athleteA, athleteB));
	}

	@Test
	public void massOfAthleteAShouldBeEqualsToAthleteB() throws Exception {
		Athlete athleteA = new Athlete(new Pair(100, 100));
		Athlete athleteB = new Athlete(new Pair(100, 100));
		Assert.assertEquals(0, new AthleteMassComparator().compare(athleteA, athleteB));
	}

	@Test
	public void shouldOrderMasses() throws Exception {
		Athlete athleteA = new Athlete(new Pair(140, 100));
		Athlete athleteB = new Athlete(new Pair(130, 100));
		Athlete athleteC = new Athlete(new Pair(120, 100));
		Athlete athleteD = new Athlete(new Pair(110, 100));
		Athlete athleteE = new Athlete(new Pair(100, 100));
		List<Athlete> athletes = Arrays.asList(athleteA, athleteB, athleteC, athleteD, athleteE);
		Collections.sort(athletes, new AthleteMassComparator());
		Assert.assertEquals(athleteA, athletes.get(4));
		Assert.assertEquals(athleteB, athletes.get(3));
		Assert.assertEquals(athleteC, athletes.get(2));
		Assert.assertEquals(athleteD, athletes.get(1));
		Assert.assertEquals(athleteE, athletes.get(0));
	}
}
