package com.amazon.second;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class AthleteStrengthComparatorTest {

	@Test
	public void strengthOfAthleteAShouldBeGreaterThanstrengthOfAthleteB() throws Exception {
		Athlete athleteA = new Athlete(new Pair(100, 140));
		Athlete athleteB = new Athlete(new Pair(100, 130));
		Assert.assertEquals(1, new AthleteStrengthComparator().compare(athleteA, athleteB));
	}

	@Test
	public void strengthOfAthleteAShouldBeLesserThanstrengthOfAthleteB() throws Exception {
		Athlete athleteA = new Athlete(new Pair(100, 130));
		Athlete athleteB = new Athlete(new Pair(100, 140));
		Assert.assertEquals(-1, new AthleteStrengthComparator().compare(athleteA, athleteB));
	}

	@Test
	public void strengthOfAthleteAShouldBeEqualsToAthleteB() throws Exception {
		Athlete athleteA = new Athlete(new Pair(100, 100));
		Athlete athleteB = new Athlete(new Pair(100, 100));
		Assert.assertEquals(0, new AthleteStrengthComparator().compare(athleteA, athleteB));
	}

	@Test
	public void shouldOrderStrengths() throws Exception {
		Athlete athleteA = new Athlete(new Pair(100, 140));
		Athlete athleteB = new Athlete(new Pair(100, 130));
		Athlete athleteC = new Athlete(new Pair(100, 120));
		Athlete athleteD = new Athlete(new Pair(100, 110));
		Athlete athleteE = new Athlete(new Pair(100, 100));
		List<Athlete> athletes = Arrays.asList(athleteA, athleteB, athleteC, athleteD, athleteE);
		Collections.sort(athletes, new AthleteStrengthComparator());
		Assert.assertEquals(athleteA, athletes.get(4));
		Assert.assertEquals(athleteB, athletes.get(3));
		Assert.assertEquals(athleteC, athletes.get(2));
		Assert.assertEquals(athleteD, athletes.get(1));
		Assert.assertEquals(athleteE, athletes.get(0));
	}
}
