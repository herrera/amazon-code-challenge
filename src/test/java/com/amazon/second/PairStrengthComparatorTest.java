package com.amazon.second;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class PairStrengthComparatorTest {

	@Test
	public void strengthOfPairAShouldBeGreaterThanstrengthOfPairB() throws Exception {
		Pair pairA = new Pair(100, 140);
		Pair pairB = new Pair(100, 130);
		Assert.assertEquals(1, new PairStrengthComparator().compare(pairA, pairB));
	}

	@Test
	public void strengthOfPairAShouldBeLesserThanstrengthOfPairB() throws Exception {
		Pair pairA = new Pair(100, 130);
		Pair pairB = new Pair(100, 140);
		Assert.assertEquals(-1, new PairStrengthComparator().compare(pairA, pairB));
	}

	@Test
	public void strengthOfAthleteAShouldBeEqualsToAthleteB() throws Exception {
		Pair pairA = new Pair(100, 100);
		Pair pairB = new Pair(100, 100);
		Assert.assertEquals(0, new PairStrengthComparator().compare(pairA, pairB));
	}

	@Test
	public void shouldOrderStrengths() throws Exception {
		Pair pairA = new Pair(100, 140);
		Pair pairB = new Pair(100, 130);
		Pair pairC = new Pair(100, 120);
		Pair pairD = new Pair(100, 110);
		Pair pairE = new Pair(100, 100);
		List<Pair> pairs = Arrays.asList(pairA, pairB, pairC, pairD, pairE);
		Collections.sort(pairs, new PairStrengthComparator());
		Assert.assertEquals(pairA, pairs.get(4));
		Assert.assertEquals(pairB, pairs.get(3));
		Assert.assertEquals(pairC, pairs.get(2));
		Assert.assertEquals(pairD, pairs.get(1));
		Assert.assertEquals(pairE, pairs.get(0));
	}
}
