package com.amazon.second;

public class Pair {

	public static int MAX_MASS = 1999999;
	public static int MAX_STRENGTH = 1999999;
	public static String MASS_EXCEPTION_MESSAGE = "Mass must be lesser than " + MAX_MASS;
	public static String STRENGTH_EXCEPTION_MESSAGE = "Mass must be lesser than " + MAX_MASS;

	private int mass;
	private int strength;

	public Pair(Integer mass, Integer strength) throws Exception {
		setMass(mass);
		setStrength(strength);
	}

	public int getMass() {
		return mass;
	}

	public void setMass(int mass) throws Exception {
		ensureLesser(mass, MAX_MASS, MASS_EXCEPTION_MESSAGE);
		this.mass = mass;
	}

	public int getStrength() {
		return strength;
	}

	public void setStrength(int strength) throws Exception {
		ensureLesser(strength, MAX_STRENGTH, STRENGTH_EXCEPTION_MESSAGE);
		this.strength = strength;
	}

	private void ensureLesser(int value, int maxValue, String exceptionMessage) throws Exception {
		if (value > maxValue) {
			throw new Exception(exceptionMessage);
		}
	}

	@Override
	public String toString() {
		return String.format("mass : '%d', strength : '%d'", getMass(), getStrength());
	}
}
