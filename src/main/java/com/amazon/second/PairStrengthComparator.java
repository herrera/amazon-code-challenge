package com.amazon.second;

import java.util.Comparator;

public class PairStrengthComparator implements Comparator<Pair> {

	@Override
	public int compare(Pair pairA, Pair pairB) {
		if (pairA.getStrength() > pairB.getStrength()) {
			return 1;
		} else if (pairA.getStrength() < pairB.getStrength()) {
			return -1;
		} else {
			return 0;
		}
	}
}
