package com.amazon.second;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static String ATHLETES_PATTERN = "^[1-9][0-9]{0,4}$";
	public static String PAIR_PATTERN = "^([0-9]{1,6}|1[0-9]{6})\\s([0-9]{1,6}|1[0-9]{6})$";

	public static void main(String[] args) throws NumberFormatException, Exception {
		BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
		int numAthletes = 0;
		while (numAthletes == 0) {
			System.out.print("Please, inform how many athletes do you have to make the tower: ");
			String numAthletesUserInput = buffer.readLine().trim();
			if (!numAthletesUserInput.matches(ATHLETES_PATTERN)) {
				System.out.println("ERROR: Invalid input. Try again some value between 1 and 99999...");
			} else {
				numAthletes = Integer.parseInt(numAthletesUserInput);
			}
		}
		System.out.println("Please inform the athletes as pairs of mass and strength, separed by a space character (e.g.: '100 100')...");
		List<Athlete> athletes = new ArrayList<Athlete>();
		while (athletes.size() < numAthletes) {
			System.out.print(String.format("Athlete %d: ", athletes.size() + 1));
			String pairUserInput = buffer.readLine().trim();
			if (!pairUserInput.matches(PAIR_PATTERN)) {
				System.out.println("ERROR: Invalid input. Try again some values between 1 and 1999999 for both mass and strength...");
			} else {
				String[] pairValues = pairUserInput.split("\\s");
				Pair athleteAttributes = new Pair(
					Integer.parseInt(pairValues[0]),
					Integer.parseInt(pairValues[1])
				);
				athletes.add(new Athlete(athleteAttributes));
			}
		}
		AthletesUtils utils = new AthletesUtils();
		System.out.println(String.format("The max number of athletes you can get on this tower is: %d", utils.getMaxTowerLength(athletes)));
	}
}
