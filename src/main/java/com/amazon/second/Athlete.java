package com.amazon.second;

public class Athlete {

	private Pair attributes;

	public Athlete(Pair attributes) {
		this.attributes = attributes;
	}

	public Pair getAttributes() {
		return attributes;
	}

	public int getMass() {
		return attributes.getMass();
	}

	public int getStrength() {
		return attributes.getStrength();
	}

	public boolean canHold(Athlete athlethe) {
		return attributes.getStrength() >= athlethe.getAttributes().getMass();
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof Athlete) {
			Athlete anotherOne = (Athlete) object;
			return (getMass() == anotherOne.getMass()) && (getStrength() == anotherOne.getStrength());
		}
		return false;
	}

	@Override
	public String toString() {
		return getAttributes().toString();
	}
}
