package com.amazon.second;

import java.util.Comparator;

public class AthleteMassComparator implements Comparator<Athlete> {

	@Override
	public int compare(Athlete athleteA, Athlete athleteB) {
		if (athleteA.getMass() > athleteB.getMass()) {
			return 1;
		} else if (athleteA.getMass() < athleteB.getMass()) {
			return -1;
		} else {
			return 0;
		}
	}
}
