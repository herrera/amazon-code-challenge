package com.amazon.second;

import java.util.Comparator;

public class AthleteStrengthComparator implements Comparator<Athlete> {

	@Override
	public int compare(Athlete athleteA, Athlete athleteB) {
		if (athleteA.getStrength() > athleteB.getStrength()) {
			return 1;
		} else if (athleteA.getStrength() < athleteB.getStrength()) {
			return -1;
		} else {
			return 0;
		}
	}
}
