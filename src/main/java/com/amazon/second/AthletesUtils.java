package com.amazon.second;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AthletesUtils {

	public static int MAX_ATHLETES = 100000;
	public static String MAX_ATHLETES_EXCEPTION_MESSAGE = "The athletes list must contains less than '" + MAX_ATHLETES + "' elements.";

	public int getMaxTowerLength(List<Athlete> athletes) throws Exception {
		if (athletes.size() > MAX_ATHLETES) {
			throw new Exception(MAX_ATHLETES_EXCEPTION_MESSAGE);
		}
		List<Athlete> orderedOnes = getAscendingOrderedByMass(athletes);
		Athlete strongestOne = getStrongestOne(athletes);
		orderedOnes.remove(strongestOne);
		int sumOfMasses = getSumOfMasses(orderedOnes);
		while ((!orderedOnes.isEmpty()) && (strongestOne.getStrength() < sumOfMasses)) {
			sumOfMasses -= orderedOnes.remove(orderedOnes.size() - 1).getMass();
		}
		return orderedOnes.size() + 1;
	}

	public List<Athlete> getAscendingOrderedByMass(List<Athlete> athletes) {
		List<Athlete> ascendingOrderedByMass = new ArrayList<Athlete>();
		ascendingOrderedByMass.addAll(athletes);
		Collections.sort(ascendingOrderedByMass, new AthleteMassComparator());
		return ascendingOrderedByMass;
	}

	public Athlete getStrongestOne(List<Athlete> athletes) {
		List<Athlete> orderedByStrength = new ArrayList<Athlete>();
		orderedByStrength.addAll(athletes);
		Collections.sort(orderedByStrength, new AthleteStrengthComparator());
		return !orderedByStrength.isEmpty() ? orderedByStrength.get(orderedByStrength.size() - 1) : null;
	}

	public int getSumOfMasses(List<Athlete> athletes) {
		int sum = 0;
		for (Athlete athlete: athletes) {
			sum += athlete.getAttributes().getMass();
		}
		return sum;
	}
}
