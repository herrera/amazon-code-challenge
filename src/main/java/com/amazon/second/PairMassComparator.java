package com.amazon.second;

import java.util.Comparator;

public class PairMassComparator implements Comparator<Pair> {

	@Override
	public int compare(Pair pairA, Pair pairB) {
		if (pairA.getMass() > pairB.getMass()) {
			return 1;
		} else if (pairA.getMass() < pairB.getMass()) {
			return -1;
		} else {
			return 0;
		}
	}
}
