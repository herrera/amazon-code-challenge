package com.amazon.first;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class NonRepeatable {

	public char firstChar(Stream input) {
		HashMap<String, Integer> mapping = new LinkedHashMap<String, Integer>();
		while (input.hasNext()) {
			char charToAssert = input.getNext();
			Integer count = mapping.get(String.valueOf(charToAssert));
			if (count != null) {
				++count;
			} else {
				count = new Integer(1);
			}
			mapping.put(String.valueOf(charToAssert), count);
		}
		char first = Stream.NULL;
		for (Entry<String, Integer> entry : mapping.entrySet()) {
			if (entry.getValue() == 1) {
				first = entry.getKey().charAt(0);
				break;
			}
		}
		return first;
	}
}
