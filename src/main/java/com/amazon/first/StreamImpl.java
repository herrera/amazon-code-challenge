package com.amazon.first;

public class StreamImpl implements Stream {

	private String sequence = "";

	public StreamImpl(String sequence) {
		if (sequence != null) {
			this.sequence = sequence;
		}
	}

	@Override
	public char getNext() {
		char next = NULL;
		if (hasNext()) {
			next = sequence.charAt(0);
			sequence = sequence.substring(1);
		}
		return next;
	}

	@Override
	public boolean hasNext() {
		return !sequence.isEmpty();
	}
}
