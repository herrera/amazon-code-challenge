package com.amazon.first;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Please, input some data: ");
		Stream userInput = new StreamImpl(buffer.readLine());
		char firstNonRepeatable = new NonRepeatable().firstChar(userInput);
		if (firstNonRepeatable != Stream.NULL) {
			System.out.print("First unrepeated char: '" + firstNonRepeatable + "'.");
		} else {
			System.out.print("Sorry, but there is no unrepeated char in your input.");
		}
	}
}
