package com.amazon.first;

public interface Stream {

	char NULL = 0;

	char getNext();

	boolean hasNext();
}
